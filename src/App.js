import React, { Component, Image } from 'react';
import enter from './static/enter.png'
import viewer_loading from './static/photosphere-logo.gif'
import PhotoSphereViewer from 'photo-sphere-viewer';
import Logo from './static/huijia_y.png'
import './App.css';
import "photo-sphere-viewer/dist/photo-sphere-viewer.min.css"

class App extends Component {

  componentDidMount() {
    this.initPSV()
  }

  initPSV(){
    let panos = [
      {
        url   : 'http://qiniu.nosalt.cn/1.jpeg',
        desc  : '惠佳门业',
        target: {
          longitude: 1.9891888250352936,
          latitude : 0.22179443327245085,
          zoom     : 10
        }
      }, {
        url   : 'http://qiniu.nosalt.cn/2.jpg',
        desc  : '大厅',
        target: {
          longitude: 1.7192923522668941,
          latitude : -0.1989310600921781,
          zoom     : 20
        }
      }, {
        url   : 'http://qiniu.nosalt.cn/3.jpeg',
        desc  : '房门',
        target: {
          longitude: 1.9891888250352936,
          latitude : 0.22179443327245085,
          zoom     : 10
        }
      },{
        url   : 'http://qiniu.nosalt.cn/4.jpg',
        desc  : '楼梯',
        target: {
          longitude: 1.9891888250352936,
          latitude : 0.22179443327245085,
          zoom     : 10
        }
      }, {
        url   : 'http://qiniu.nosalt.cn/5.jpg',
        desc  : '二楼',
        target: {
          longitude: 1.9891888250352936,
          latitude : 0.22179443327245085,
          zoom     : 10
        }
      }, {
        url   : 'http://qiniu.nosalt.cn/6.jpeg',
        desc  : '二楼-左',
        target: {
          longitude: 1.9891888250352936,
          latitude : 0.22179443327245085,
          zoom     : 10
        }
      }, {
        url   : 'http://qiniu.nosalt.cn/7.jpg',
        desc  : '二楼-右',
        target: {
          longitude: 1.9891888250352936,
          latitude : 0.22179443327245085,
          zoom     : 10
        }
      }, {
        url   : 'http://qiniu.nosalt.cn/8.jpeg',
        desc  : '阳台推拉门',
        target: {
          longitude: 1.9891888250352936,
          latitude : 0.22179443327245085,
          zoom     : 10
        }
      },    
    ];
  
    let markersList={
      damen:[{
          id       : 'to-dating',
          tooltip  : {
            content : '欢迎光临',
            position: 'top',
          },
          latitude : -0.1989310600921781,
          longitude: 1.7192923522668941,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
      dating:[{
          id       : 'to-fangmen',
          tooltip  : {
            content : '看房门',
            position: 'top',
          },
          latitude : -0.5136788017580733,
          longitude: 0.19302006602835906,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-louti',
          tooltip  : {
            content : '到楼梯口',
            position: 'top',
          },
          latitude : -0.38843074531342614,
          longitude: 1.8841934513147451,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-damen',
          tooltip  : {
            content : '谢谢惠顾',
            position: 'top',
          },
          latitude : -0.6168816693889445,
          longitude: 4.537988732327043,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
        fangmen:[{
          id       : 'to-dating',
          tooltip  : {
            content : '到大厅',
            position: 'top',
          },
          latitude : -0.4534522848819509,
          longitude: 1.6743574133702164,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-louti',
          tooltip  : {
            content : '到楼梯口',
            position: 'top',
          },
          latitude : -0.4574651567041692,
          longitude: 6.212352013761593,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
        louti:[{
          id       : 'to-fangmen',
          tooltip  : {
            content : '看房门',
            position: 'top',
          },
          latitude : -0.6545150326248619,
          longitude: 4.782916570861493,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-erlou',
          tooltip  : {
            content : '上二楼',
            position: 'top',
          },
          latitude : -0.222067052686864,
          longitude: 6.25020512611484,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-dating',
          tooltip  : {
            content : '到大厅',
            position: 'top',
          },
          latitude : -0.6951492283294152,
          longitude: 3.4200163468153963,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
        erlou:[{
          id       : 'to-louti',
          tooltip  : {
            content : '下楼',
            position: 'top',
          },
          latitude : -0.7488388651411912,
          longitude: 3.1367049501264286,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-fangmen-left',
          tooltip  : {
            content : '室内房门左',
            position: 'top',
          },
          latitude : -0.3937871264092858,
          longitude: 4.737267426945329,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-fangmen-right',
          tooltip  : {
            content : '室内房门右',
            position: 'top',
          },
          latitude : -0.3222932974015753,
          longitude: 5.798808413182419,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-yangtaimen',
          tooltip  : {
            content : '阳台推拉门',
            position: 'top',
          },
          latitude : -0.26889496648177813,
          longitude: 1.44388187362117,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
        fangmen_left:[{
          id       : 'to-erlou',
          tooltip  : {
            content : '二楼楼梯口',
            position: 'top',
          },
          latitude : -0.20687323360499454,
          longitude: 5.440131467642686,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-fangmen-right',
          tooltip  : {
            content : '室内房门右',
            position: 'top',
          },
          latitude : -0.21603424869107357,
          longitude: 4.424919830138034,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
        fangmen_right:[{
          id       : 'to-erlou',
          tooltip  : {
            content : '二楼楼梯口',
            position: 'top',
          },
          latitude : -0.46681548996485667,
          longitude: 0.2888800120646277,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        },{
          id       : 'to-fangmen-left',
          tooltip  : {
            content : '室内房门左',
            position: 'top',
          },
          latitude : -0.41051159078333144,
          longitude: 1.4129471460414305,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
        yangtaimen:[{
          id       : 'to-erlou',
          tooltip  : {
            content : '二楼楼梯口',
            position: 'top',
          },
          latitude : -0.533462448677116,
          longitude: 0.4231854507667258,
          image    : enter,
          width    : 48,
          height   : 48,
          anchor   : 'bottom center',
          data     : {
            deletable: true,
          },
        }],
    }

    let container = document.getElementById('viewer')
  
    const PSV = new PhotoSphereViewer({
      container      : container,
      panorama       : panos[0].url,
      caption        : panos[0].desc,
      loadingImg     : viewer_loading,
      // longitude_range : [-8 * Math.PI / 8, 8 * Math.PI / 8],
      // latitude_range  : [-3 * Math.PI / 4, 3 * Math.PI / 4],
      autorotateSpeed: '-2rpm',
      defaultZoomLvl : 1,
      fisheye        : false,
      moveSpeed      : 1.1,
      default_long: Math.PI / 8 * 4,
      default_lat: Math.PI / 12,
      min_fov: 30,
      max_fov: 90,
      default_fov: 90,
  //    touchmoveTwoFingers: true,
  //    mousemoveHover: true,
      navbar         : [
        'zoom', 
        'markers',
        // {
        //   title    : 'Change image',
        //   className: 'custom-button',
        //   content  : '🔄',
        //   onClick  : (function() {
        //     var i = 0;
        //     var loading = false;
  
        //     return function() {
        //       if (loading) {
        //         return;
        //       }
  
        //       i = 1 - i;
        //       loading = true;
        //       PSV.hud.clearMarkers();
  
        //       PSV.setPanorama(panos[i].url, panos[i].target, true)
        //         .then(function() {
        //           PSV.navbar.setCaption(panos[i].desc);
        //           loading = false;
        //         });
        //     };
        //   }()),
        // },
        // {
        //   title    : 'Random position',
        //   className: 'custom-button',
        //   content  : '🔀',
        //   onClick  : function() {
        //     PSV.animate({
        //       longitude: (Math.random() - 0.5) * 3 / 2 * Math.PI,
        //       latitude : (Math.random() - 0.5) * 3 / 4 * Math.PI,
        //       zoom     : Math.random() * 60 + 20
        //     }, 1500);
        //   },
        // },
        'caption',  'fullscreen',
      ],
      markers : (function() {
        var a = markersList.damen;
        return a;
      }()),
    });
  
    function resetMarker(markers) {
      PSV.hud.clearMarkers();
      markers.forEach(element => {
        PSV.hud.addMarker(element)
      });
    }
  
    PSV.on('click', function(e, data) {
      // console.log('click-long', data.longitude);
      // console.log('click-lat', data.latitude);
      // PSV.hud.addMarker({
      //   id       : '#' + Math.random(),
      //   tooltip  : 'Generated marker',
      //   longitude: data.longitude,
      //   latitude : data.latitude,
      //   image    : '../../assets/pin1.png',
      //   width    : 32,
      //   height   : 32,
      //   anchor   : 'bottom center',
      //   data     : {
      //     deletable: true,
      //   },
      // });
    });
  
    PSV.on('select-marker', function(marker, id, dblclick) {
      console.log(marker)
      if (marker.id === "to-damen") {
        PSV.setPanorama(panos[0].url, panos[0].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[0].desc);
            resetMarker(markersList.damen)
            // this.loading = false;
        });
      } else if (marker.id === "to-dating") {
        PSV.setPanorama(panos[1].url, panos[1].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[1].desc);
            resetMarker(markersList.dating)
            //this.loading = false;
        });
      } else if (marker.id === "to-fangmen") {
        PSV.setPanorama(panos[2].url, panos[2].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[2].desc);
            resetMarker(markersList.fangmen)
            //this.loading = false;
        });
      } else if (marker.id === "to-louti") {
        PSV.setPanorama(panos[3].url, panos[3].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[3].desc);
            resetMarker(markersList.louti)
            //this.loading = false;
        });
      } else if (marker.id === "to-erlou") {
        PSV.setPanorama(panos[4].url, panos[4].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[4].desc);
            resetMarker(markersList.erlou)
            //this.loading = false;
        });
      }  else if (marker.id === "to-fangmen-left") {
        PSV.setPanorama(panos[5].url, panos[5].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[5].desc);
            resetMarker(markersList.fangmen_left)
            //this.loading = false;
        });
      }  else if (marker.id === "to-fangmen-right") {
        PSV.setPanorama(panos[6].url, panos[6].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[6].desc);
            resetMarker(markersList.fangmen_right)
            //this.loading = false;
        });
      }  else if (marker.id === "to-yangtaimen") {
        PSV.setPanorama(panos[7].url, panos[7].target, true)
            .then(function() {
            //PSV.navbar.setCaption(panos[7].desc);
            resetMarker(markersList.yangtaimen)
            //this.loading = false;
        });
      }
    });
  
    PSV.on('over-marker', function(e, marker) {
      console.log('over', marker.id);
    });
  
    PSV.on('leave-marker', function(e, marker) {
      console.log('leave', marker.id);
    });
  
    PSV.on('select-marker-list', function(e, marker) {
      console.log('select-list', marker.id);
    });
  
    PSV.on('goto-marker-done', function(e, marker) {
      console.log('goto-done', marker.id);
    });
  }
  
  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img className= "logo" src={Logo}/>
          <div className="name-wrapper">
            <div className="name">惠佳门业(崇武店)</div>
            <div className="name-pingyin">huijiamenye</div>
          </div>
        </div>
        <div id="viewer" className="viewer"/>
        <div className="footer">
          <div className='footer-info'>联系热线：0595-87687786 13805996802</div>
          <div className='footer-info'>地址：惠安县崇武镇五峰村建行西侧惠佳门业</div>
        </div>
      </div>
    );
  }
}

export default App;
